package enteros.dyv.logica;

import java.util.ArrayList;

import javax.swing.JTextField;

import enteros.logica.Multiplicar;

public class DivideyVenceras implements Multiplicar {
	
	private String log;
	private ArrayList<String> numerosParaOperar;
	
	@Override
	public String divideyvenceras(ArrayList<JTextField> numeros) {
		
		this.log = "Iniciando Proceso\n";
		this.log = this.log + "Inicicializando variables...\n";
		numerosParaOperar = new ArrayList<String>();
		
		for (JTextField txt_temp : numeros) {
			String tmp = txt_temp.getText();
			if(!tmp.isEmpty()){
				numerosParaOperar.add(tmp);
			}
		}
			
		/** Multiplicaciones alternativas **/
		
		if(numerosParaOperar.size() > 1 ){
			this.log = this.log + "Numeros capturados\n";
			this.log = this.log + numerosParaOperar.toString();
			double resultado = 1;
			for (String numero : numerosParaOperar) {
				int limiteSuperior = numero.length()/2;
				int limiteSuperiorTmp = limiteSuperior ; 
				if( (( (double) numero.length()/2 ) - limiteSuperior ) >= 0.5){
					limiteSuperiorTmp = limiteSuperior + 1;
				}
				int numero1 = (int) (Integer.parseInt(numero.substring(0, limiteSuperior)) * Math.pow(10,limiteSuperiorTmp));
				int numero2 = Integer.parseInt(numero.substring( limiteSuperior ));
				this.log = this.log + "\n Numero : "+ numero1 + " + "+ numero2 ;
				resultado = resultado * ( numero1 + numero2 );
			}
			this.log = this.log + "\nResultado Obtenido " + resultado;
		}else{
			this.log = this.log + "No se encontraron numeros para operar\n";
		}
		
		return this.log;
	}

	@Override
	public String usual() {
		//No se implementa por esta logica
		return null;
	}

}
