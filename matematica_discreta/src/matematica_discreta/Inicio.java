package matematica_discreta;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;

import matematica_discreta.vistas.Acerca;
import matematica_discreta.vistas.MatricesUsual;
import matematica_discreta.vistas.MultiplicacionDV;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.Window.Type;
import java.awt.Font;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import java.awt.SystemColor;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class Inicio extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inicio frame = new Inicio();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Inicio() {
		setTitle("Matematicas Discretas 2016");
		setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 600);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmAcercaDe = new JMenuItem("Acerca de");
		mnArchivo.add(mntmAcercaDe);
		
		JMenuItem mntmCerrar = new JMenuItem("Cerrar");
		mnArchivo.add(mntmCerrar);
		
		JMenu mnMatrices = new JMenu("Multiplicaci\u00F3n Matrices");
		menuBar.add(mnMatrices);
		
		JMenuItem mntmUsual = new JMenuItem("Usual");
		mnMatrices.add(mntmUsual);
		
		JMenuItem mntmDivideYVencers = new JMenuItem("Divide y vencer\u00E1s");
		mnMatrices.add(mntmDivideYVencers);
		
		JMenu mnMultiplicacinEnteros = new JMenu("Multiplicaci\u00F3n Enteros");
		menuBar.add(mnMultiplicacinEnteros);
		
		JMenuItem mntmUsual_1 = new JMenuItem("Usual");
		mnMultiplicacinEnteros.add(mntmUsual_1);
		
		JMenuItem mntmDivideYVencers_1 = new JMenuItem("Divide y vencer\u00E1s");
		mnMultiplicacinEnteros.add(mntmDivideYVencers_1);
		
		/**
		 * Metodos de acceso
		 * **/
		
		//Cerrar
		mntmCerrar.addActionListener(new ActionListener() {
		       public void actionPerformed(ActionEvent ae){
		           //Cuadro de confirmacion de salida de la aplicaci�n
		    	   
		    	   String[] opciones = {"No", "Si, deseo cerrar la aplicaci�n"};
		    	   
		           int opcion = JOptionPane.showOptionDialog(
		                                  getParent()                      //componente
		                                , "�Desea salir de la aplicaci�n?" // Mensaje
		                                , "Cerrar Matem�ticas Discretas"   // Titulo en la barra del cuadro
		                                , JOptionPane.DEFAULT_OPTION       // Tipo de opciones
		                                , JOptionPane.INFORMATION_MESSAGE  // Tipo de mensaje (icono)
		                                , null                             // Icono (ninguno)
		                                , opciones                         // Opciones personalizadas
		                                , null                             // Opcion por defecto
		                              );
		           
		           if(opcion == 1){
		        	   System.exit(EXIT_ON_CLOSE);
		           }
		       } 
		    });
		
		//Acerca
		mntmAcercaDe.addActionListener(new ActionListener() {
		       public void actionPerformed(ActionEvent ae){
		           Acerca panelAcerca = new Acerca();
		           panelAcerca.setLocation(5, 5);
		           contentPane.removeAll();
		           contentPane.add(panelAcerca , BorderLayout.CENTER);
		           contentPane.revalidate();
		           contentPane.repaint();
		       } 
		    });
		
		//Multiplicacion enteros divide y venceras
		mntmDivideYVencers_1.addActionListener(new ActionListener() {
		       public void actionPerformed(ActionEvent ae){
		           MultiplicacionDV panelAcerca = new MultiplicacionDV();
		           panelAcerca.setLocation(5, 5);
		           contentPane.removeAll();
		           contentPane.add(panelAcerca , BorderLayout.CENTER);
		           contentPane.revalidate();
		           contentPane.repaint();
		       } 
		    });
		
		mntmUsual.addActionListener(new ActionListener() {
		       public void actionPerformed(ActionEvent ae){
		           MatricesUsual panelAcerca = new MatricesUsual();
		           panelAcerca.setLocation(5, 5);
		           contentPane.removeAll();
		           contentPane.add(panelAcerca , BorderLayout.CENTER);
		           contentPane.revalidate();
		           contentPane.repaint();
		       } 
		    });
		
		
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.windowBorder);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
	}

}
