package matematica_discreta.vistas;

import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

import matematica_discreta.imagenes.ImagenLogo;
import javax.swing.border.LineBorder;
import java.awt.SystemColor;
import javax.swing.BoxLayout;

public class Acerca extends JPanel {

	/**
	 * Create the panel.
	 */
	public Acerca() {
		setForeground(SystemColor.menu);
		setBorder(new LineBorder(null, 20));
		setLayout(new GridLayout(6, 0, 0, 0));
		
		JLabel lblNewLabel = new JLabel("Fundaci\u00F3n Universitaria Konrad Lorenz");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
		add(lblNewLabel);
		
		
		ImagenLogo panel = new ImagenLogo();
		add(panel);
		
		JLabel lblNewLabel_1 = new JLabel("Integrantes");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		add(lblNewLabel_1);
		
		JLabel lblJhonFreddyPuentes = new JLabel("Jhon Freddy Puentes");
		add(lblJhonFreddyPuentes);
		
		JLabel label = new JLabel("Aric Dayan Gutierrez Sanchez  - C�digo 506162030");
		add(label);
		
		JLabel materia = new JLabel("Matem�ticas Discretas");
		add(materia);

	}

}
