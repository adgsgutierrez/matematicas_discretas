package matematica_discreta.vistas;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Component;
import java.awt.GridBagConstraints;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.Insets;
import javax.swing.JEditorPane;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import matrices.logica.Matrices;
import matrices.usual.logica.Usual;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;

public class MatricesUsual extends JPanel {
	private JTable table;
	private int contadorMatriz = 1;
	private ArrayList<JTable> matrices = new ArrayList<>();
	private ArrayList<JLabel> titulos = new ArrayList<>();
	
	/**
	 * Create the panel.
	 */
	
	
	public MatricesUsual() {
		setBorder(new EmptyBorder(15, 15, 15, 15));
		
		String[][] inicializador = {
				{"",""},{"",""}
		};
		
		String[] title = {"",""};	

		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 1.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblMultiplicacinDeMatrices = new JLabel("Multiplicaci\u00F3n de Matrices");
		GridBagConstraints gbc_lblMultiplicacinDeMatrices = new GridBagConstraints();
		gbc_lblMultiplicacinDeMatrices.insets = new Insets(0, 0, 5, 5);
		gbc_lblMultiplicacinDeMatrices.anchor = GridBagConstraints.WEST;
		gbc_lblMultiplicacinDeMatrices.gridx = 1;
		gbc_lblMultiplicacinDeMatrices.gridy = 0;
		add(lblMultiplicacinDeMatrices, gbc_lblMultiplicacinDeMatrices);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridwidth = 3;
		gbc_panel.gridheight = 7;
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 1;
		add(panel, gbc_panel);
	
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{42, 0, 138, 0};
		gbl_panel.rowHeights = new int[]{107, 32, 0};
		gbl_panel.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblMatrizA = new JLabel("Matriz "+contadorMatriz);
		GridBagConstraints gbc_lblMatrizA = new GridBagConstraints();
		gbc_lblMatrizA.gridwidth = 1;
		gbc_lblMatrizA.insets = new Insets(0, 0, 5, 5);
		gbc_lblMatrizA.gridx = 0;
		gbc_lblMatrizA.gridy = 0;
		panel.add(lblMatrizA, gbc_lblMatrizA);
		
		matrices.add(new JTable(inicializador , title));
		matrices.get((matrices.size()-1)).setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.gridwidth = 2;
		gbc_table.gridx = 1;
		gbc_table.gridy = 0;
		panel.add(matrices.get((matrices.size()-1)), gbc_table);
		
		JButton btnAdicionarMatriz = new JButton("Adicionar Matriz");
		btnAdicionarMatriz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				titulos.add( new JLabel("Matriz "+(contadorMatriz+1)));
				GridBagConstraints gbc_lblMatrizA1 = new GridBagConstraints();
				gbc_lblMatrizA1.gridwidth = 1;
				gbc_lblMatrizA1.insets = new Insets(0, 0, 5, 5);
				gbc_lblMatrizA1.gridx = 0;
				gbc_lblMatrizA1.gridy = contadorMatriz;
				panel.add(titulos.get(titulos.size()-1), gbc_lblMatrizA1);
				
				matrices.add(new JTable(inicializador , title));
				matrices.get((matrices.size()-1)).setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
				GridBagConstraints gbc_table1 = new GridBagConstraints();
				gbc_table1.gridwidth = 2;
				gbc_table1.gridx = 1;
				gbc_table1.gridy = contadorMatriz;
				panel.add(matrices.get((matrices.size()-1)), gbc_table1);

				contadorMatriz = contadorMatriz + 1;
				/** Vuelve a pintar **/
		   		
		   		revalidate();
		        repaint();
				
			}
		});
		GridBagConstraints gbc_btnAdicionarMatriz = new GridBagConstraints();
		gbc_btnAdicionarMatriz.gridwidth = 4;
		gbc_btnAdicionarMatriz.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAdicionarMatriz.insets = new Insets(0, 0, 5, 0);
		gbc_btnAdicionarMatriz.gridx = 4;
		gbc_btnAdicionarMatriz.gridy = 1;
		add(btnAdicionarMatriz, gbc_btnAdicionarMatriz);
		
		JButton btnNuevaColumna = new JButton("Nueva Columna");
		btnNuevaColumna.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int max = matrices.size();
				
				String[] list = new String [max];
				for(int i = 0 ; i < max ; i ++) {
					list [i] = "Matriz "+(i+1);
				}
				JComboBox jcb = new JComboBox(list);
				jcb.setEditable(true);
				JOptionPane.showMessageDialog( getParent() , jcb, "Seleccione la Matriz para adicionar la columna", JOptionPane.QUESTION_MESSAGE);
				int matriz = jcb.getSelectedIndex();
								
				/** Capturando la cantidad de Columnas**/
								
				int columnasActuales = matrices.get(matriz).getColumnCount();
				int filasActuales = matrices.get(matriz).getRowCount();
								
				String[][] inicializador = new String[filasActuales][(columnasActuales+1)];
				String[] title = new String[(columnasActuales+1)];	
				
				for(int i = 0 ; i < (columnasActuales+1) ; i++){
					title[i] = "";
					for(int j=0; j < filasActuales ; j ++){
						inicializador[j][i] = "";
					}
				}
				
				matrices.get(matriz).setModel(new DefaultTableModel(inicializador, title));
											
				/** Vuelve a pintar **/
		   						
		   		revalidate();
		        repaint();
			}
		});
		GridBagConstraints gbc_btnNuevaColumna = new GridBagConstraints();
		gbc_btnNuevaColumna.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNuevaColumna.insets = new Insets(0, 0, 5, 5);
		gbc_btnNuevaColumna.gridx = 4;
		gbc_btnNuevaColumna.gridy = 2;
		add(btnNuevaColumna, gbc_btnNuevaColumna);
		
		JButton btnNuevaFila = new JButton("Nueva Fila");
		GridBagConstraints gbc_btnNuevaFila = new GridBagConstraints();
		gbc_btnNuevaFila.gridwidth = 3;
		gbc_btnNuevaFila.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNuevaFila.insets = new Insets(0, 0, 5, 0);
		gbc_btnNuevaFila.gridx = 5;
		gbc_btnNuevaFila.gridy = 2;
		add(btnNuevaFila, gbc_btnNuevaFila);
		btnNuevaFila.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int max = matrices.size();
				
				String[] list = new String [max];
				for(int i = 0 ; i < max ; i ++) {
					list [i] = "Matriz "+(i+1);
				}
				JComboBox jcb = new JComboBox(list);
				jcb.setEditable(true);
				JOptionPane.showMessageDialog( getParent() , jcb, "Seleccione la Matriz para adicionar la Fila", JOptionPane.QUESTION_MESSAGE);
				int matriz = jcb.getSelectedIndex();
								
				/** Capturando la cantidad de Columnas**/
								
				int columnasActuales = matrices.get(matriz).getColumnCount();
				int filasActuales = matrices.get(matriz).getRowCount();
								
				String[][] inicializador = new String[(filasActuales+1)][(columnasActuales+1)];
				String[] title = new String[(columnasActuales)];	
				
				for(int i = 0 ; i < (columnasActuales) ; i++){
					title[i] = "";
					for(int j=0; j < (filasActuales+1) ; j ++){
						inicializador[j][i] = "";
					}
				}
				
				matrices.get(matriz).setModel(new DefaultTableModel(inicializador, title));
											
				/** Vuelve a pintar **/
		   						
		   		revalidate();
		        repaint();
			}
		});
				JEditorPane editorPane = new JEditorPane();
				editorPane.setEditable(false);
		GridBagConstraints gbc_editorPane = new GridBagConstraints();
		gbc_editorPane.gridheight = 4;
		gbc_editorPane.gridwidth = 4;
		gbc_editorPane.fill = GridBagConstraints.BOTH;
		gbc_editorPane.insets = new Insets(0, 0, 5, 0);
		gbc_editorPane.gridx = 4;
		gbc_editorPane.gridy = 4;
		add(editorPane, gbc_editorPane);


		JTextPane textPane = new JTextPane();
		GridBagConstraints gbc_textPane = new GridBagConstraints();
		gbc_textPane.gridwidth = 4;
		gbc_textPane.insets = new Insets(0, 0, 0, 5);
		gbc_textPane.fill = GridBagConstraints.BOTH;
		gbc_textPane.gridx = 4;
		gbc_textPane.gridy = 8;
		add(textPane, gbc_textPane);
		
		
		JButton button = new JButton("Iniciar Proceso");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(matrices.size() > 1){
					Matrices matriz = new Usual();
					editorPane.setText(matriz.usual(matrices));
					textPane.setText(editorPane.getText());
				}else{
					editorPane.setText("No se puede operar una sola matriz");
					textPane.setText(editorPane.getText());
				}
					
			}
		});
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.gridwidth = 4;
		gbc_button.fill = GridBagConstraints.HORIZONTAL;
		gbc_button.insets = new Insets(0, 0, 5, 0);
		gbc_button.gridx = 4;
		gbc_button.gridy = 3;
		add(button, gbc_button);
		

	}

}
