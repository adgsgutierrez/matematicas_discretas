package matematica_discreta.vistas;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JButton;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.JEditorPane;
import javax.swing.border.EtchedBorder;

import enteros.dyv.logica.DivideyVenceras;
import enteros.logica.Multiplicar;

import javax.swing.border.EmptyBorder;

public class MultiplicacionDV extends JPanel {
	
	private int ultimaposicion;
	private ArrayList<JTextField> arregloComponentes;
	
	/**
	 * Create the panel.
	 */
	public MultiplicacionDV() {
		setBorder(new EmptyBorder(15, 15, 15, 15));
		arregloComponentes = new ArrayList<>();
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblIngreseLosNumeros = new JLabel("Ingrese los numeros que desea operar");
		GridBagConstraints gbc_lblIngreseLosNumeros = new GridBagConstraints();
		gbc_lblIngreseLosNumeros.gridwidth = 3;
		gbc_lblIngreseLosNumeros.insets = new Insets(0, 0, 5, 5);
		gbc_lblIngreseLosNumeros.gridx = 1;
		gbc_lblIngreseLosNumeros.gridy = 1;
		add(lblIngreseLosNumeros, gbc_lblIngreseLosNumeros);
		
		arregloComponentes.add(new JTextField());
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 2;
		add(arregloComponentes.get((arregloComponentes.size()-1)), gbc_textField);
		arregloComponentes.get((arregloComponentes.size()-1)).setColumns(10);
		
		arregloComponentes.add(new JTextField());
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 2;
		gbc_textField_1.gridy = 3;
		add(arregloComponentes.get((arregloComponentes.size()-1)), gbc_textField_1);
		arregloComponentes.get((arregloComponentes.size()-1)).setColumns(10);
		
		this.ultimaposicion = 3;
		
		JLabel lblSolucinImplementada = new JLabel("Soluci\u00F3n Implementada");
		GridBagConstraints gbc_lblSolucinImplementada = new GridBagConstraints();
		gbc_lblSolucinImplementada.gridwidth = 2;
		gbc_lblSolucinImplementada.anchor = GridBagConstraints.WEST;
		gbc_lblSolucinImplementada.insets = new Insets(0, 0, 5, 5);
		gbc_lblSolucinImplementada.gridx = 5;
		gbc_lblSolucinImplementada.gridy = 1;
		add(lblSolucinImplementada, gbc_lblSolucinImplementada);
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setEditable(false);
		GridBagConstraints gbc_editorPane = new GridBagConstraints();
		gbc_editorPane.gridwidth = 3;
		gbc_editorPane.gridheight = 4;
		gbc_editorPane.insets = new Insets(0, 0, 5, 5);
		gbc_editorPane.fill = GridBagConstraints.BOTH;
		gbc_editorPane.gridx = 4;
		gbc_editorPane.gridy = 2;
		add(editorPane, gbc_editorPane);
		
		JButton btnOtroNumero = new JButton("Otro n\u00FAmero");
		GridBagConstraints gbc_btnOtroNumero = new GridBagConstraints();
		gbc_btnOtroNumero.gridwidth = 2;
		gbc_btnOtroNumero.insets = new Insets(0, 0, 5, 5);
		gbc_btnOtroNumero.gridx = 4;
		gbc_btnOtroNumero.gridy = 6;
		add(btnOtroNumero, gbc_btnOtroNumero);
		
		btnOtroNumero.addActionListener(new ActionListener() {
		       public void actionPerformed(ActionEvent ae){
		    	arregloComponentes.add(new JTextField());
		    	ultimaposicion = ultimaposicion + 1;
		   		
		    	/** Agrega el campo de texto **/
		    	GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		   		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		   		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		   		gbc_textField_1.gridx = 2;
		   		gbc_textField_1.gridy = ultimaposicion;
		   		add(arregloComponentes.get((arregloComponentes.size()-1)), gbc_textField_1);
		   		arregloComponentes.get((arregloComponentes.size()-1)).setColumns(10);
		   				
		   		/** Vuelve a pintar **/
		   		
		   		revalidate();
		        repaint();
		       }
		});

		JButton btnEjecutarAlgoritmo = new JButton("Ejecutar Algoritmo");
		GridBagConstraints gbc_btnEjecutarAlgoritmo = new GridBagConstraints();
		gbc_btnEjecutarAlgoritmo.insets = new Insets(0, 0, 5, 5);
		gbc_btnEjecutarAlgoritmo.gridx = 6;
		gbc_btnEjecutarAlgoritmo.gridy = 6;
		add(btnEjecutarAlgoritmo, gbc_btnEjecutarAlgoritmo);
		
		btnEjecutarAlgoritmo.addActionListener(new ActionListener() {
		       public void actionPerformed(ActionEvent ae){
		    	   Multiplicar multiplicar = new DivideyVenceras();
		    	   editorPane.setText(multiplicar.divideyvenceras(arregloComponentes));
		    	   
		       }});
		
		
	}

}
