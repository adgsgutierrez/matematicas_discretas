package matrices.usual.logica;

import java.util.ArrayList;

import javax.swing.JTable;

import matrices.logica.Matrices;

public class Usual implements Matrices{
	
	private String log;
	private ArrayList<Integer[][]> matrices;
	@Override
	public String divideyvenceras() {
		// Este metodo no se implementa aqui
		return null;
	}

	@Override
	public String usual(ArrayList<JTable> matricesInput) {
		this.log = "Iniciando proceso...";
		matrices = new ArrayList<>();
		//Validando matrices 
		try{
			this.log = this.log + "\nValidando Matrices...";
			for (JTable tabla : matricesInput) {
				int columnas = tabla.getColumnCount();
 				int filas    = tabla.getRowCount();
				Integer [][] tablaTemporal = new Integer [filas][columnas];
				for (int i = 0 ; i < filas ; i ++){
					for (int j = 0 ; j < columnas ; j ++){
						tablaTemporal[i][j] = Integer.parseInt((String) tabla.getValueAt(i, j));
					}
				}
				matrices.add(tablaTemporal);
			}
			this.log = this.log + "\nValidación de caractéres exitosa";
			this.log = this.log + "\nIniciando multiplicación de "+matrices.size()+" matrices\n";
			
			//Iniciando multiplicacion 
			
			Integer [][] matrizResultante = null;
			
			for (Integer [][] matriz : matrices) {
				if(matrizResultante != null){
					matrizResultante = multiplicarMatrices(matrizResultante , matriz);
				}else{
					matrizResultante = matriz;
				}
			}
			
			this.log = this.log + "\nMultiplicación Exitosa";
			this.log = this.log + "\nMatriz resultante :";
			this.log = this.log + "\n";
			for(int i = 0 ; i < matrizResultante.length ; i++){
				for(int j = 0 ; j < matrizResultante[0].length ; j++){
					this.log = this.log + matrizResultante[i][j] +"\t";
				}
				this.log = this.log + "\n";
			}
		}catch(NumberFormatException e){
			System.out.println("Error "+ e);
			this.log = this.log + "\nSe encontró una letra o especio en una matriz";
		}
		//System.out.println("Array Matrices "+matrices.toString());
		return this.log;
	}

	
	private Integer[][] multiplicarMatrices(Integer[][] matrizA , Integer[][] matrizB){
		Integer [][] matrizResultante = null ;
		
		int filasMatrizA = matrizA.length;
		int columnasMatrizA = matrizA[0].length;
		
		int filasMatrizB = matrizB.length;
		int columnasMatrizB = matrizB[0].length;
		
		matrizResultante = new Integer[filasMatrizA][columnasMatrizB];

		for(int i = 0 ; i< filasMatrizA; i ++){
			for(int j = 0 ;j < columnasMatrizB ; j ++){
				matrizResultante[i][j] = 0;
			}	
		}
		
		for (int x=0; x < filasMatrizA; x++) {
			  for (int y=0; y < columnasMatrizB; y++) {
			    for (int z=0; z < columnasMatrizA; z++) {
			    	//log = log + matrizResultante [x][y] +" + ("+matrizA[x][z]+" X "+matrizB[z][y]+")";
			    	matrizResultante [x][y] += matrizA[x][z]*matrizB[z][y]; 
			    }
			    //log = log + "\t";
			  }
			  //log = log + "\n";
			}
		
		
		return matrizResultante;
	}
}
